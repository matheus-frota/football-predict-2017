# Importando bibliotecas
import pandas as pd
import numpy as np

def dataset():
    # Criando os dataframes
    df_atletas = pd.read_csv('/home/jovyan/work/workspace/data/2017/Atletas.csv')
    df_clubes = pd.read_csv('/home/jovyan/work/workspace/data/2017/Clubes.csv')
    df_partidas = pd.read_csv('/home/jovyan/work/workspace/data/2017/Partidas.csv')
    df_scouts = pd.read_csv('/home/jovyan/work/workspace/data/2017/Scouts.csv')
    df_pontuacoes = pd.read_csv('/home/jovyan/work/workspace/data/new/pontuacao.csv')
    
    # Rótulo dos nomes
    rotulos = {Id:nome for Id,nome in zip(df_clubes['ID'],df_clubes['Nome']) if Id != 1}
    
    # Substituindo valores dos times no df_Partidas
    df_partidas = df_partidas.replace(rotulos)
    
    # Substituindo valores dos times no df_Scouts
    df_scouts = df_scouts.replace(rotulos)
    
    # Colocando em ordem por rodadas o dataset de partidas
    df_partidas = df_partidas.sort_values('Rodada',ascending = True)
    
    # Saldo de gol em cada jogo
    df_partidas['SGJogoAtualCasa']= np.array(df_partidas['PlacarCasa']) - np.array(df_partidas['PlacarVisitante'])
    df_partidas['SGJogoAtualVisitante'] = np.array(df_partidas['PlacarVisitante']) - np.array(df_partidas['PlacarCasa'])
    
    return df_partidas

# Função geradora do saldo de gols de todos os times
def sg_value(df):
    # Chamando df_partida para manipulação
    df_partidas = dataset()
    # criando dataframe para receber os Saldo de gols
    test1 = pd.DataFrame()
    # Laço para percorrer todos os times do campeonato
    for time in df['CasaID'].unique():
        # Recebe o time joggando em casa
        a = df[df['CasaID'] == time]
        # Recebe o time jogando como visitante
        b = df[df['VisitanteID'] == time]
        # Concatena os dois dataframes
        test = pd.concat([a,b])
        # Coloca em ordem crescente as rodadas
        test = test.sort_values('Rodada',ascending = True)
        # Variável usada para armazenar os saldo de gols
        aux = []
        # Contador usado para dizer qual rodada é o saldo de gols
        i = 1
        # Laço usado para passar de linha a linha e pegas os salds de gols
        for _,row in test.iterrows():
            # Condição usada para pegar apenas os saldos quando o time tiver jogando em casa
            if row['CasaID'] == time:
                if row['Rodada'] == 1:
                    aux.append((i,0))
                    i+=1
                aux.append((i,row['SGJogoAtualCasa']))
                i+=1
            # Condição usada para pegar apenas o saldo quando tiver jogando fora de casa
            elif row['VisitanteID'] == time:
                if row['Rodada'] == 1:
                    aux.append((i,0))
                    i+=1
                aux.append((i,row['SGJogoAtualVisitante']))
                i+=1
        # Variável usada para armazenar a nova lista de saldo de gols por rodadas        
        new_aux = []
        # Laço uzado para fazer a operação de somatorio: SG rodada[i] -> SG rodada Atual + SG rodada0 Anterior
        for i in aux:
            if i[0] == 1:
                new_aux.append(i[1])
                ant = i[1]
            atual = i[1]
            soma = ant + atual
            new_aux.append(soma)
            ant = soma
        test1['SG{}'.format(time)] = new_aux
        #print(time)
        #print(new_aux[-1])
        new_aux = []
        aux = []
        i = 1
    test1['Rodada'] = list(range(0,40))
    return test1

def aplicacao_mudancas(df, sg, nome):
    # Variável que recebe valor dos saldos de gols dos times jogando em Casa
    C = []
    # Variável que recebe o valor dos saldos de gols dos times jogando fora de casa
    V = []
    # Laço usado para pegar os saldos das partidas do nosso banco de dados
    for _,row in df.iterrows():
        # Variável que armazena o valor da rodada
        r = row['Rodada']
        # Condicional para pular a execução da linha de rodada 0
        if r == 0:
            continue
        # Variável recebendo o time que joga em casa
        timeC = row['CasaID']
        # Variável recebendo o time que joga fora de casa
        timeV = row['VisitanteID']
        # Variável filtrando o dataset para apenas os valores da rodada analisado
        sg_test = sg[sg['Rodada'] == r]
        # Adicionando o valor do saldo do time jogando em casa naquela rodada
        C.append(list(sg_test['{}{}'.format(nome,timeC)])[0])
        # Adicionando o valor do saldo do time jogando fora de casa naquela rodada
        V.append(list(sg_test['{}{}'.format(nome,timeV)])[0])
        
    return C,V

def rend_time(df):
    test1 = pd.DataFrame()
    # Laço para percorrer todos os times do campeonato
    for time in df['CasaID'].unique():
        # Recebe o time joggando em casa
        a = df[df['CasaID'] == time]
        # Recebe o time jogando como visitante
        b = df[df['VisitanteID'] == time]
        # Concatena os dois dataframes
        test = pd.concat([a,b])
        # Coloca em ordem crescente as rodadas
        test = test.sort_values('Rodada',ascending = True)
        # Variável auxiliar
        aux = []
        for _,row in test.iterrows():
            # Condição usada para pegar apenas o time quando tiver jogando em casa
            if row['CasaID'] == time:
                # Variável que armazena o saldo de gols atual do jogo
                x = row['SGJogoAtualCasa']
                # Condição para comparar, se o saldo for positivo é vitoria, negativo é derrota e zero é empate.
                if row['Rodada'] == 1:
                    aux.append(0)
                elif x > 0:
                    aux.append('v')
                elif x < 0:
                    aux.append('d')
                elif x == 0:
                    aux.append('e')
            # Condição usada para pegar apenas o time quando tiver jogando fora de casa
            # Funciona da mesma forma que a condição passada
            elif row['VisitanteID'] == time:
                y = row['SGJogoAtualVisitante']
                if row['Rodada'] == 1:
                    aux.append(0)
                elif y > 0:
                    aux.append('v')
                elif y < 0:
                    aux.append('d')
                elif y == 0:
                    aux.append('e')
        # Variável auxiliar que armazena os historico do time até 5 partidas anteriores
        new_aux = []
        # Laço que percorre a lista com os valores da partida
        for i,e in enumerate(aux):
            # Condicional para armazenar os estados e fazer a seguinte coisa:
            # Exemplo:
            # rodada resultado jogo atual historico
            # 1           V                  0
            # 2           e                  v
            # 3           d                  ve
            # e assim sucessivamente até atingir hisotico do tamanho 5 ue é o máximo.
            if i <= 1:
                new_aux.append(e)
                ant = e
            elif i > 5:
                new_aux.append(ant[1:] + e)
                ant = new_aux[-1]
            elif i <= 5 and i > 1:
                new_aux.append(ant+e)
                ant = new_aux[-1]
        
        # Calculo da média ponderada para definir o rendimento
        # Média ponderada:Pesos[ V -> 3, D -> 3, E -> 1]/ pesos = 7
        new_aux2 = []
        for i,e in enumerate(new_aux):
            if i == 0:
                new_aux2.append(e)
            else:
                V = e.count('v')
                E = e.count('e')
                D = e.count('d')
        
                media_pond = (3*V + 1*E + (-3)*D)/(3 + (-3) + 1)
        
                new_aux2.append(round(media_pond,2))
        # Cria um dataframe com o rendimento de cada time separadamente
        test1['Rendimento{}'.format(time)] = new_aux2
        new_aux = []
        new_aux2 = []
        aux = []
    test1['Rodada'] = list(range(1,39))
    return test1

def resultados_jogos(df):
    # Laço para retirar os resultados do time jogando em casa
    # Indices Vitoria = 1, Derrota = 2, empate = 3
    resultados_casa = []
    resultados_visitante = []

    for _,row in df.iterrows():
        if row['PlacarCasa'] > row['PlacarVisitante']:
            resultados_casa.append('vitoria')
            resultados_visitante.append('derrota')
        elif row['PlacarCasa'] < row['PlacarVisitante']:
            resultados_casa.append('derrota')
            resultados_visitante.append('vitoria')
        elif row['PlacarCasa'] == row['PlacarVisitante']:
            resultados_casa.append('empate')
            resultados_visitante.append('empate')
    return resultados_casa,resultados_visitante

def main():
    # Importando dataset para manipulação
    df = dataset()
    # Criando dataframe com os valores de saldo de gols de cada time no campeonato todo.
    sg_times = sg_value(df)
    # Criando variáveis com os valores dos saldo de gols dos times jogando em casa e visitantes
    sgC, sgV = aplicacao_mudancas(df, sg_times, 'SG')
    # Adicionando esses valores no dataframe inicial
    df['SGAtualCasa'] = sgC
    df['SGAtualVisitante'] = sgV
    # Criando dataframe com os valores de rendimento de cada time em cada rodada.
    rend_times = rend_time(df)
    # Criando variáveis com os valores de rendimento dos times jogando em casa e visitantes nos ultimos 5 jogos.
    rendC, rendV = aplicacao_mudancas(df, rend_times, 'Rendimento')
    # Adicionando esses valores no dataframe inicial
    df['RendimentoCasa'] = rendC
    df['RendimentoVisitante'] = rendV
    # Criando variáveis com valores dos resultados dos jogos tanto na ótica jogando em casa quanto jogando como visitante.
    resultadoC, resultadoV = resultados_jogos(df)
    # Adicionando mudanças no dataframe
    df['ResultadosCasa'] = resultadoC
    df['ResultadosVisitante'] = resultadoV
    # Removendo featuures que não fazem parte do dataframe
    df = df.drop(['ID','SGJogoAtualCasa','SGJogoAtualVisitante','PlacarCasa','PlacarVisitante'], axis = 1)
    return df