import pandas as pd #Manipulação do dataframe
import data_preparation as dp # Importando dataset
import numpy as np # Biblioteca de calculo cientifico
from sklearn.model_selection import KFold, StratifiedKFold,GridSearchCV, cross_val_predict # Biblioteca de separação em treino e teste e validação cruzada
from sklearn.preprocessing import OneHotEncoder # Biblioteca do oe hot encoder
from sklearn.preprocessing import Normalizer# Biblioteca para normalização
from sklearn.ensemble import RandomForestClassifier# Biblioteca do random forest
import sklearn.metrics
from sklearn.preprocessing import LabelEncoder

# Função para preparar as variáveis que vão ser utilizadas para treinar o modelo
def XY():
    # Dataframe para teste do modelo
    df = dp.main()
    # Separando em variaveis dependentes e independentes
    # Variável independentes
    X = df.drop(['ResultadosCasa','ResultadosVisitante','Rodada'],axis = 1)
    # Variáveis dependentes
    Y1 = df['ResultadosCasa']
    Y2 = df['ResultadosVisitante']
    # Aplicando one hot encoder
    X_encoded = pd.get_dummies(X, dtype = bool)
    # Normalizando valores numéricos
    colunas = ['SGAtualCasa','SGAtualVisitante','RendimentoCasa','RendimentoVisitante']
    scaler = Normalizer().fit(X_encoded[colunas])
    X_encoded[colunas] = scaler.transform(X_encoded[colunas])
    return X_encoded,Y1,Y2



# Função que retorna performance do modelo por cross validation
def metricas_performance():
    X,Y1,Y2 = XY()
    bl = RandomForestClassifier(class_weight = 'balanced', criterion = 'gini', max_features = 'sqrt', n_estimators = 40)
    # Treinando o modelo
    predictionsY1 = cross_val_predict(bl, X, Y1, cv=5)
    print('Performance do modelo preditor para os times que jogaram em casa')
    print(sklearn.metrics.classification_report(Y1, predictionsY1))
    predictionsY2 = cross_val_predict(bl, X, Y2, cv=5)
    print('Performance do modelo preditor para os times que jogaram fora de casa')
    print(sklearn.metrics.classification_report(Y2, predictionsY2))
    
# Função para se entrar o modelo e retornar resultado da predição
def test_model(entrada,tipo):
    X,Y1,Y2 = XY()
    bl = baseline()
    # Treinando o modelo
    if tipo == 'c':
        test = bl.fit(X,Y1)
        return test.predict(entrada)
    elif tipo == 'v':
        test = bl.fit(X,Y2)
        return test.predict(entrada)