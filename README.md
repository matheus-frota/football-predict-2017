# football-predict-2017

## Motivação:

Esse projeto foi um compartilhado como desafio que tinha como objetivo predizer o resultado dos jogos que aconteceram em 2017 no brasileirão série A.

## Pré-requisitos

* [Docker](https://docs.docker.com/install/)
* [Docker-Compose](https://docs.docker.com/compose/install/) 

## Guia de instalação:

Com o docker e o docker-compose instalados a única coisa que falta é mudar o path de onde está o projeto.

![image](/uploads/6f4eca3a73882e7479a36d192e8a5e85/image.png)

Feito isso, basta utilizar o comando dentro do diretório onde o projeto está: `docker-compose up`

## Estrutura do projeto:

O projeto seguiu a seguinte estrutura de contrução:

1. **Data Preparation**
2. **Data Analysis**
3. **Modeling**

## Referências:

* [What I’ve learnt predicting soccer matches with machine learning](https://medium.com/@neoyipeng/what-ive-learnt-predicting-soccer-matches-with-machine-learning-b3f8b445149d)
* [Prevendo resultado de partidas de futebol com Machine Learning e o CartolaFC](https://www.linkedin.com/pulse/prevendo-resultado-de-partidas-futebol-com-o-usando-r-santiago/)
* [Como fazer uma boa análise de jogo de futebol para apostar](https://clubedaposta.com/como-apostar/boa-analise-de-jogo/)
* [5 Fatores Para Análise De Uma Partida](http://blog.investimentofutebol.com/dicas-de-trading-esportivo/analise-de-uma-partida/)
* [3 Fatores Para Análise Antes de Uma Partida](http://blog.investimentofutebol.com/dicas-de-trading-esportivo/3-fatores-para-analise-antes-de-uma-partida/)

## Licença:

Este projeto está licenciado sob os termos da licença do MIT. Veja o arquivo LICENSE.