# CHANGELOG

Esse documento contém o planejamento diário do projeto.

A cada dia colocarei documentado o que foi feito e o que planejo fazer no próximo dia.


# Dia 29/03


## Tarefas realizadas

Antes de iniciar qualquer coisa voltada para o problema em si, a utilização do docker tem o intuito de facilitar a portabilidade do projeto. 

Então, foi visto:

*  O que é o Docker
*  O que é um Container
*  Modulos do kernel utilizados pelo Docker
*  Alguns comandos importantes, como: docker run, docker rm, docker stats, docker ps, docker images, etc...

E outras que incluiam:

* Mandar e-mail com dúvidas ao Felipe e Bruno
* COnfiguração inicial do repositório
* Criação de uma docmentação inicial

## Tarefas propostas

1. Continuação do curso Docker epi 7 à epi 14 no mínimo
2. Entender o problema proposto e descrevê-lo
3. Começar a construção do dataset de análise


# Dia 30/03

## Tarefas realizadas

Foram realizados as seguintes tarefas, referentes as metas traçadas no dia anterior.

* Conclusão do curso Docker
* Configuração do ambiente de trabalho utilizando Docker Compose

## Tarefas propostas

1. Terminar as tarefas propostas no dia anterior que não deram para concluir.


# Dia 31/03

## Tarefas realizadas

Conseguimos concluir as tarefas propostas no dia 30 e pesquisar um pouco mais sobre o problema.

* Estudo do problema proposto, buscando em artigos relacionados tudo que poderia ajudar.
* Inicio da construção do dataset com o repositório disponibilizado pelo Felipe e Bruno
* Obtenção de novos bancos de dados dos campeonatos do ano de 2014 à 2017, com mais informações

## Tarefas propostas

* Manipulação dos datasets obtidos, para a construção de um dataset de análise inicial
* Acrescentar novos diretórios no repositório para uma melhor organização do projeto

Caso acabar os dois acima, irei iniciar uma análise descritiva do problema.


# Dia 01/04

## Tarefas realizadas

* Conclusão da função de processamento dos dados para formação do dataset(Versão Temporária).
* Pesquisa feita para ver quais fatores influenciam no resultado dos jogos.

## Tarefas pendêntes

* Organizar repositório

## Tarefas propostas

* Terminar as tarefas pendentes;
* Análise descritiva do banco de dados do campeonato de 2016;
* Criação de um modelo Baseline;

# Dia 02/04

## Tarefas realizadas

* Conclusão das tarefas pendentes
* Conclusão da função para pré-processar os dados
* Concluido apenas de uma parte da exploração dos dados de 2016
* Organização do repositório

## Tarefas pendêntes

* Ajeitar função de processamento dos dados, que está dando erro no  banco de dados de 2015
* Iniciar o modelo baseline
* Terminar a análise exploratória dos dados

## Tarefas propostas

* Conclusão das tarefas pendentes

# Dia 03/04

Hoje criei o primeiro modelo baseline utilizando o algorítmo Random Forest do Sklearn, mas infelizmente as variáveis eram muito ruins e fez com que esse modelo classificasse
todos como a classe que tinha mais ocorrência. Em decorrência disso irei construir novas features.

## Tarefa proposta

* Estudar mais sobre o problema proposto, pesquisar mais em artigos referêntes a apostas em jogos de futebol.
* Construir esqueleto do projeto que será utilizado.

# Dia 04/04 à 07/04

## Tarefas realizadas

* Todas as tarefas propostas no dia 03 foram realizadas

**Obs: Será criado um arquivo texto contendo todas as referências que utilizamos para a construção desse modelo.**

## Tarefas propostas

* Construção de novas features;
* Análise exploratória
* Criação do modelo Baseline
* Organização do repositório
* Passar para um arquivo .py as funções desenvolvidas no ambiente jupyter
* Guardar imagens de comparação da melhoria do modelo anterior para esse com novas features
* Organizar Readme para explicar o fncionamento e os pontos principais do projeto
* Organizar o código em geral para melhor compreenção na leitura.

## Tarefas concluidas

* Todas as tarefas propostas foram concluidas
